package messages;

import java.net.Socket;

import tools.Constants;
import tools.IOStreams;
import tools.Tools;
import beans.FileRead;
import beans.User;

import com.sun.org.apache.xml.internal.security.utils.Base64;
import command.Command;
import command.ReadFileCommand;

import configuration.Configuration;

/**
 * ReadFile provides the method to ask for a decrypted file to a TC. 
 * @author tranvan
 */
public class ReadFile {
	public static String readFile(Boolean IsTcell, String FileGID, User user) {
		return readFile(IsTcell, FileGID, user, Configuration.getConfiguration().getProperty("appHomePath"));
	}

	/**
	 * readfile Reads the file described by a fileGID 
	 * @param FileGID the global ID of the file 
	 */
	public static String readFile(Boolean IsTcell, String FileGID, User user, String folderWhereToSend) {
		folderWhereToSend = (folderWhereToSend.substring(folderWhereToSend.length() - 1) == "/") ? folderWhereToSend : folderWhereToSend + "/";

		String TCellIP = user.getTCellIP();
		int TcellPort = user.getPort();
	
		try {
			Socket socket = new Socket(TCellIP, TcellPort);
			/* Creation of the stream */
			IOStreams stream = new IOStreams(socket);

			/* Send the command */
			Command readFileCmd = new ReadFileCommand(Constants.CMD_READ_FILE, FileGID);
			stream.getOutputStream().writeObject(readFileCmd);

			FileRead file = (FileRead) stream.getInputStream().readObject();
								
			if (IsTcell)
				file.setFilePath(Configuration.getConfiguration().getProperty("tcellPath") + Tools.getFileName(file.getFilePath()));
			else
				file.setFilePath(folderWhereToSend + Tools.getFileName(file.getFilePath()));
								
			Tools.writeFileFromPath(file.getFilePath(), Base64.decode(file.getFile()));
			System.out.println("File available at " + file.getFilePath());
			
			stream.close();
			
			return file.getFilePath();
		} catch (Exception ex) {
			System.err.println("This file cannot be read");
		}
		
		return "";
	}
}

