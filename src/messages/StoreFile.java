package messages;

import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.net.Socket;
import java.security.PublicKey;

import javax.crypto.SecretKey;

import tools.Constants;
import tools.IOStreams;
import tools.SymKeyTools;
import tools.Tools;
import beans.User;

import com.sun.org.apache.xml.internal.security.utils.Base64;
import command.StoreFileCommand;

import configuration.Configuration;
import cryptoTools.AsymmetricEncryption;
import cryptoTools.SymmetricEncryption;

/**
 * StoreFile is the class used to store a file in a TC.
 * It provides the methods to send an encrypted file to a TC followed by an encrypted message containing the description of the file.
 * The file is encrypted by AES and the message is encrypted by RSA, with the TC's public key
 * 
 * @author Majdi Ben Fredj,TORKHANI Rami
 * 
 */
public class StoreFile {
	public static String storeFile(String fileToSendPath, User user) {
		return storeFile(fileToSendPath, user, Configuration.getConfiguration().getProperty("appHomePath") + Constants.TMP_FILES);
	}
	
	public static String storeFile(String fileToSendPath, User user, String folderWhereToSend) {
		int clientID = Integer.parseInt(Configuration.getConfiguration().getProperty("myGID"));
		return storeFile(fileToSendPath, user, folderWhereToSend, clientID);
	}
	
	public static String storeFile(String fileToSendPath, User user, String folderWhereToSend, int clientID) {
		folderWhereToSend = (folderWhereToSend.substring(folderWhereToSend.length() - 1) == "/") ? folderWhereToSend : folderWhereToSend + "/";

		/**
		 * Establishes the connection with the TCELL and send to file with the store message.
		 * @param fileToSendPath the path of the file to send
		 * @param user the addressee user
		 */
		try {
			/* The socket used to send the file and the messages */
			Socket socket;

			/* Extract the fileName from the path */
			String fileName = Tools.getFileName(fileToSendPath);
			
			/* Creation of the temporary directory */
			if (!Tools.createDir(folderWhereToSend))
				return  "";

			System.out.println("File is being sent to TCell...");
			String destIP = user.getTCellIP();
			int destPort = user.getPort();

			/* Creates a stream socket and connects it to the specified port number at the specified IP address */
			socket = new Socket(destIP, destPort);

			/* Creation of the stream */
			IOStreams stream = new IOStreams(socket);

			/* Encryption of the file */
			SecretKey sKey = SymKeyTools.GenSymKey();
			byte[] iv = new byte[Constants.IV_LENGTH];

			String encryptFilePath = folderWhereToSend + "(" + user.getUserGID() + "pour" + clientID + ")_" + Constants.SYM_ENCR_FILE_NAME + fileName;
			byte[] encryptFile = encryptFile(fileToSendPath, sKey, iv, encryptFilePath);
			
			PublicKey pubKey = Tools.stringToPublicKey(user.getPubKey());
			byte[] encrSkey= AsymmetricEncryption.encryptBlockByBlock(sKey.getEncoded(), pubKey);
			
			StoreFileCommand storeCmd = new StoreFileCommand(Constants.CMD_STORE_FILE, Base64.encode(encrSkey), Base64.encode(iv), encryptFilePath, Base64.encode(encryptFile));
			stream.getOutputStream().writeObject(storeCmd); //send command
			
			//receive status from the server
			int status = stream.getInputStream().readInt();
			Tools.interpretStatus(status);

			stream.close();
			socket.close();
			
			// Renvoie le path du chemin encrypt� si tout s'est bien pass�
			return encryptFilePath;
		} catch (Exception ex) {
			System.err.println("ERROR : store file has failed");
			return "";
		}
	}

	/**
	 * Encrypts the file.
	 * @param fullFileName the path of the file to encrypt
	 * @param sKey the SecretKey 
	 * @param iv the initialization vector
	 * @return the encrypted file
	 */
	public static byte[] encryptFile(String fullFileName, SecretKey sKey, byte[] iv, String fullPath) {
		byte [] encbytes = null;
		
		try {
			String filename = Tools.getFileName(fullFileName);
			FileInputStream	is = new FileInputStream(fullFileName); 
			FileOutputStream eos = new FileOutputStream(fullPath);
			
			//Encrypt the file
			int dataSize = is.available();
			byte[] inbytes = new byte[dataSize];
			is.read(inbytes);
			
			//Calling the Symmetric Encryption for encrypting the file
			SymmetricEncryption.encryptFile( inbytes,sKey, eos, iv );
			eos.flush();

			//Read the Outputstream
			encbytes = Tools.readFileFromPath(fullPath);
			if(encbytes == null) {
				System.err.println("ERROR : The file cannot be encrypted");
				is.close();
				eos.close();
				return null;
			}

			is.close();
			eos.close();

			System.out.println("Encryption is done..."+filename+" is now ready to be sent");
		} catch (Exception ex) {
			ex.printStackTrace();
		}

		return encbytes;
	}
}