package ihm;

import java.io.File;
import java.util.ArrayList;

import api.ClientAPI;
import beans.User;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.scene.control.Label;
import javafx.scene.control.TreeItem;
import javafx.scene.control.TreeView;
import javafx.scene.input.MouseEvent;
import javafx.stage.DirectoryChooser;
import javafx.stage.Stage;

public class Tools {
	
	// Renvoie un array listant les fichiers d'un r�pertoire sous forme d'arbre
	private static ArrayList<TreeItem<String>> manageDirectoryDependency(File directory) {
		File[] filesInDir = directory.listFiles();

		// Liste des fichiers et sous dossiers du dossier
		ArrayList<File> fileList = new ArrayList<File>();
		ArrayList<File> subdirectoryList = new ArrayList<File>();
		for (File f : filesInDir) {
			if (f.isDirectory())
				subdirectoryList.add(f);
			else
				fileList.add(f);
		}

		ArrayList<TreeItem<String>> mergedDirectoryFileList = new ArrayList<TreeItem<String>>();

		// Pour les dossiers, on r�cup�re les fichiers qui y sont contenus
		for (File dir : subdirectoryList) {
			ArrayList<TreeItem<String>> children = manageDirectoryDependency(dir);
			
			TreeItem<String> currentDir = new TreeItem<String>(dir.getPath());
			currentDir.getChildren().addAll(children);
			mergedDirectoryFileList.add(currentDir);
		}
		for (File file : fileList) {
			mergedDirectoryFileList.add(new TreeItem<String>(file.getPath()));
		}

		return mergedDirectoryFileList;
	}

	// EventHandler pour mettre � jour le rootItem en fonction du r�pertoire choisi
	public static EventHandler<ActionEvent> selectFolder(TreeItem<String> rootItem, Label statusBarLabel, DirectoryChooser directoryChooser, Stage primaryStage) {
		return new EventHandler<ActionEvent>() {
			@Override
			public void handle(ActionEvent event) {
				File dir = directoryChooser.showDialog(primaryStage);
				updateFolderView(rootItem, statusBarLabel, dir);
			}
		};
	}
	
	// Mise � jour du listing de fichiers
	public static void updateFolderView(TreeItem<String> rootItem, Label statusBarLabel, File dir) {
		rootItem.getChildren().clear();
		
		if (dir != null & dir.exists()) {
			rootItem.setValue(dir.getPath());
			rootItem.getChildren().addAll(manageDirectoryDependency(dir));
			statusBarLabel.setText("Dossier chang� vers "+dir.getPath());
		}
	}
	public static void updateFolderView(TreeItem<String> rootItem, Label statusBarLabel) {
		updateFolderView(rootItem, statusBarLabel, new File(rootItem.getValue()));
	}
	
	// EventHandler pour d�chiffrer un fichier
	public static EventHandler<MouseEvent> decryptEvent(TreeItem<String> rootItemSource, TreeItem<String> rootItemDest,
			TreeView<String> treeSource, Label statusBarLabel, TreeView<String> treeDest, int userGID, User user) {
		return new EventHandler<MouseEvent>() {
			@Override
			public void handle(MouseEvent evt) {
				String errorMessage = "Veuillez s�lectionner un fichier � d�crypter et un dossier destination";
				
				// V�rification qu'une source � d�crypter est s�lectionn�e
				if (treeDest.getSelectionModel().isEmpty()) {
					statusBarLabel.setText(errorMessage);
					System.err.println(errorMessage);
					return;
				}
				
				// R�cup�ration du chemin du fichier � d�crypter
				File decryptFile = new File(treeDest.getSelectionModel().getSelectedItem().getValue());

				// R�cup�ration du dossier vers lequel on souhaite d�crypter
				File destinationDirectory;
				if (treeSource.getSelectionModel().isEmpty()) // Aucun dossier de destination n'est s�lectionn�
					destinationDirectory = new File(rootItemSource.getValue());
				else // Un dossier de destination est s�lectionn�
					destinationDirectory = new File(treeSource.getSelectionModel().getSelectedItem().getValue());
				
				// V�rification que les s�lections existent encore (au cas o�)
				if (!decryptFile.exists() || !destinationDirectory.exists()) {
					statusBarLabel.setText("Une des s�lections a �t� supprim�e entre temps ...");
					System.err.println("Une des s�lections a �t� supprim�e entre temps ...");
					return;
				}
				
				// Si le dossier de destination est un fichier, on utilise le dossier parent
				if (!destinationDirectory.isDirectory())
					destinationDirectory = new File(rootItemSource.getValue());
				
				// V�rification de la validit� du fichier � d�crypter / du dossier destination
				if (!decryptFile.isFile() || !destinationDirectory.isDirectory()) {
					statusBarLabel.setText(errorMessage);
					System.err.println(errorMessage);
					return;
				}

				// D�cryptage du fichier s�lectionn� vers le dossier s�lectionn�
				System.out.println("D�cryptage de "+decryptFile.getPath()+" dans le dossier "+destinationDirectory.getPath());
				try {
					String userGIDSender = decryptFile.getName().split("pour")[0].substring(1);
					String decryptFilePath = ClientAPI.readFile(userGIDSender+"|"+decryptFile.getPath(), user, destinationDirectory.getPath());
					statusBarLabel.setText("Fichier d�chiffr� � l'adresse "+decryptFilePath);
				} catch (Exception exc) {
					exc.printStackTrace();
				}
				
				// On met � jour les vues des dossiers
				updateFolderView(rootItemSource, statusBarLabel);
				updateFolderView(rootItemDest, statusBarLabel);
			}
		};
	}
	
	// EventHandler pour chiffrer un fichier
	public static EventHandler<MouseEvent> encryptEvent(TreeItem<String> rootItemSource, TreeItem<String> rootItemDest,
			TreeView<String> treeSource, Label statusBarLabel, TreeView<String> treeDest, int userGID, User user, ObservableList<String> selectedUsers) {
		return new EventHandler<MouseEvent>() {
			@Override
			public void handle(MouseEvent evt) {
				String errorMessage = "Veuillez s�lectionner un fichier � encrypter et un dossier destination";
				
				// V�rification qu'une source � encrypter est s�lectionn�e
				if (treeSource.getSelectionModel().isEmpty()) {
					statusBarLabel.setText(errorMessage);
					System.err.println(errorMessage);
					return;
				}
				
				// R�cup�ration du chemin du fichier � encrypter
				File encryptFile = new File(treeSource.getSelectionModel().getSelectedItem().getValue());

				// R�cup�ration du dossier vers lequel on souhaite encrypter
				File destinationDirectory;
				if (treeDest.getSelectionModel().isEmpty()) // Aucun dossier de destination n'est s�lectionn�
					destinationDirectory = new File(rootItemDest.getValue());
				else // Un dossier de destination est s�lectionn�
					destinationDirectory = new File(treeDest.getSelectionModel().getSelectedItem().getValue());

				// V�rification que les s�lections existent encore (au cas o�)
				if (!encryptFile.exists() || !destinationDirectory.exists()) {
					statusBarLabel.setText("Une des s�lections a �t� supprim�e entre temps ...");
					System.err.println("Une des s�lections a �t� supprim�e entre temps ...");
					return;
				}
				
				// Si le dossier de destination est un fichier, on utilise le dossier parent
				if (!destinationDirectory.isDirectory())
					destinationDirectory = new File(rootItemDest.getValue());

				// V�rification de la validit� du fichier � encrypter / du dossier destination
				if (!encryptFile.isFile() || !destinationDirectory.isDirectory()) {
					statusBarLabel.setText(errorMessage);
					System.err.println(errorMessage);
					return;
				}

				// Encryptage du fichier s�lectionn� vers le dossier s�lectionn� pour tous les utilisateurs s�lectionn�s
				System.out.println("Encryptage de "+encryptFile.getPath()+" dans le dossier "+destinationDirectory.getPath());

				ArrayList<Integer> clientIDs = new ArrayList<Integer>();
				clientIDs.add(userGID);
				for (String clientShared : selectedUsers) {
					int clientID = Integer.parseInt(clientShared.split("Client")[1]);
					if (!clientIDs.contains(clientID))
						clientIDs.add(clientID);
				}

				for (int clientID : clientIDs) {
					try {
						String encryptFilePath;
						if (clientID == userGID) {
							encryptFilePath = ClientAPI.storeFile(encryptFile.getPath(), user, destinationDirectory.getPath());
							statusBarLabel.setText("Fichier chiffr� � l'adresse "+encryptFilePath);
						}
						else {
							encryptFilePath = ClientAPI.storeFile(encryptFile.getPath(), user, destinationDirectory.getPath(), clientID);
							ClientAPI.shareFile(userGID + "|" + encryptFilePath, clientID, user);
							statusBarLabel.setText("Fichier chiffr� pour l'utilisateur "+clientID+" � l'adresse "+encryptFilePath);
						}
					} catch (Exception exc) {
						exc.printStackTrace();
					}
				}
				
				// On met � jour les vues des dossiers
				updateFolderView(rootItemSource, statusBarLabel);
				updateFolderView(rootItemDest, statusBarLabel);
			}
		};
	}
	
	// R�cup�ration de la liste des utilisateurs (� savoir les clients TCell)
	public static ArrayList<String> getUserList() {
		File path = new File("/home/user/SIPD/machines");
		ArrayList<String> users = new ArrayList<String>();
		if (path.isDirectory()) {
			File[] usersInPath = path.listFiles();
			for (File f : usersInPath) {
				String name = f.getName();
				if (name.startsWith("Client"))
					users.add(name);
			}
		}
		return users;
	}

}
