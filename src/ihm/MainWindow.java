package ihm;

import java.io.File;
import java.security.PublicKey;
import java.util.ArrayList;

import beans.User;
import configuration.Configuration;
import cryptoTools.KeyManager;
import javafx.application.Application;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.SelectionMode;
import javafx.scene.control.TreeItem;
import javafx.scene.input.MouseEvent;
import javafx.scene.control.TreeView;
import javafx.scene.control.TitledPane;
import javafx.scene.layout.HBox;
import javafx.scene.control.Label;
import javafx.scene.control.ListView;
import javafx.scene.layout.VBox;
import javafx.stage.DirectoryChooser;
import javafx.stage.Stage;
import tools.Constants;

public class MainWindow extends Application {

	public static VBox root = new VBox();
	public static HBox listsView = new HBox();
	public static TreeItem<String> rootItemSource = new TreeItem<String> ("Select a folder to display");
	public static TreeItem<String> rootItemDest = new TreeItem<String> ("Select a folder to display");
	public static TreeView<String> treeSource = new TreeView<String> (rootItemSource);
	public static TreeView<String> treeDest = new TreeView<String> (rootItemDest);
	public static Label statusBarLabel = new Label("Successfully launched");

	private static User user;

	@Override
	public void start(Stage primaryStage) throws Exception {
		// Récupération des informations pour le token
		int userGID = Integer.parseInt(Configuration.getConfiguration().getProperty("myGID"));
		String tCellIP = Configuration.getConfiguration().getProperty("myIP");
		int port = Integer.parseInt(Configuration.getConfiguration().getProperty("myPort"));

		// Chargement de la clé publique de l'utilisateur
		try {
			String KeyPath = Configuration.getConfiguration().getProperty("keyPath");
			KeyManager keygen = new KeyManager();
			String publicKeyPath = KeyPath + Constants.PUB_KEY_PREFIX + userGID + Constants.KEY_EXT;
			PublicKey pubKey = keygen.LoadPublicKey(publicKeyPath, Constants.RSA_ALG);
			String pubkey = keygen.PublicKeyToString(pubKey);

			user = new User(userGID, tCellIP, port, pubkey);
		} catch (Exception e) {
			e.printStackTrace();
		}

		Tools.updateFolderView(rootItemDest, statusBarLabel, new File("/home/user/SIPD/machines/Tcell"+userGID));
		
		final DirectoryChooser directoryChooser = new DirectoryChooser();
		directoryChooser.setTitle("Select a directory");
		directoryChooser.setInitialDirectory(new File(System.getProperty("user.home")));

		
		// --------- WINDOW LAYOUT ----------------
		
		
		// Panel gauche (source)
		Button selectSource = new Button("Set source directory");
		
		VBox paneLeft = new VBox(10, treeSource, selectSource);
		paneLeft.setAlignment(Pos.CENTER);
		
		TitledPane leftTitledPane = new TitledPane("Source directory", paneLeft);
		leftTitledPane.setCollapsible(false);

		
		// Panel droit (crypté)
		//Button selectDest = new Button("Set destination directory");
		
		VBox paneRight = new VBox(10, treeDest);
		paneRight.setAlignment(Pos.CENTER);
		
		TitledPane rightTitledPane = new TitledPane("Secured directory", paneRight);
		rightTitledPane.setCollapsible(false);
        
		
		// ListView des utilisateurs
		ArrayList<String> users = Tools.getUserList();
		ObservableList<String> usersList = FXCollections.observableArrayList(users);
		
		ListView<String> usersView = new ListView<String>(usersList);
		usersView.setMaxWidth(150);
		usersView.setMaxHeight(200);
		usersView.getSelectionModel().setSelectionMode(SelectionMode.SINGLE);
		usersView.setOnMouseClicked(e -> {
			String tcell = "Tcell" + usersView.getSelectionModel().getSelectedItem().substring(6, 8);
			Tools.updateFolderView(rootItemDest, statusBarLabel, new File("/home/user/SIPD/machines/" + tcell));
	    });
	    
	    // Liste des utilisateurs sélectionnés
        ObservableList<String> selectedUsers =  usersView.getSelectionModel().getSelectedItems();

        // Panel des boutons
        Button leftArrow = new Button("← Decrypt");
        Button rightArrow = new Button("Encrypt →");
        
        VBox paneButtons = new VBox(10, rightArrow, usersView, leftArrow);
		paneButtons.setAlignment(Pos.CENTER);
		
		// StatusBar
		HBox statusBar = new HBox(5, statusBarLabel);
		statusBarLabel.setPadding(new Insets(5, 10, 5, 10));
		statusBar.setStyle("-fx-border-color: lightGray");

		
		// --------- EVENT HANDLERS ----------------
		
		
		// Event handler pour le panel gauche
		EventHandler<ActionEvent> eventSource = Tools.selectFolder(rootItemSource, statusBarLabel, directoryChooser, primaryStage);
		selectSource.setOnAction(eventSource);
		rootItemSource.setExpanded(true);

		// Event handler pour le panel droit
		//EventHandler<ActionEvent> eventDest = Tools.selectFolder(rootItemDest, directoryChooser, primaryStage);
		//selectDest.setOnAction(eventDest);
        rootItemDest.setExpanded(true);
		
		// Traitement de l'événement lors d'un clic sur la flèche gauche - décryptage
		EventHandler<MouseEvent> leftArrowEventHandler = Tools.decryptEvent(rootItemSource, rootItemDest, treeSource, statusBarLabel, treeDest, userGID, user);
		leftArrow.addEventFilter(MouseEvent.MOUSE_CLICKED, leftArrowEventHandler);
		
		// Traitement de l'événement lors d'un clic sur la flèche droite - encryptage
		EventHandler<MouseEvent> rightArrowEventHandler = Tools.encryptEvent(rootItemSource, rootItemDest, treeSource, statusBarLabel, treeDest, userGID, user, selectedUsers);
		rightArrow.addEventFilter(MouseEvent.MOUSE_CLICKED, rightArrowEventHandler);
		
		
		// --------- LAYOUT LINKING  ----------------
		

		// Resize les panels
		leftTitledPane.setMinWidth(350);
		paneButtons.setMinWidth(150);
		rightTitledPane.setMinWidth(350);
		paneLeft.setMinHeight(515);
		paneButtons.setMinHeight(515);
		paneRight.setMinHeight(515);
		
		// Regroupement des panels gauche, boutons et droit
		listsView.setSpacing(20);
		listsView.setAlignment(Pos.CENTER);
		listsView.getChildren().addAll(leftTitledPane, paneButtons, rightTitledPane);
		
		// On ajoute les enfants à la racine
		root.setPadding(new Insets(10));
		root.setSpacing(10);
		root.getChildren().addAll(listsView, statusBar);

		Scene scene = new Scene(root, 1000, 600);
		primaryStage.setTitle("Duroppubakkusu - Client "+userGID);
		primaryStage.setScene(scene);
		primaryStage.show();
	}

	public static void main(String[] args) {
		Application.launch(args);
	}
}
