package api;

import java.io.IOException;
import java.net.UnknownHostException;
import java.util.ArrayList;

import messages.GetFileDesc;
import messages.ReadFile;
import messages.ShareFile;
import messages.StoreFile;
import beans.User;





/**
 * USER API
 * 
 */
public class ClientAPI
{
	public static String storeFile(String fileID, User user) {
		return StoreFile.storeFile(fileID, user);
	}
	public static String storeFile(String fileID, User user, String destinationFolder) {
		return StoreFile.storeFile(fileID, user, destinationFolder);
	}
	public static String storeFile(String fileID, User user, String destinationFolder, int clientIDToShare) {
		return StoreFile.storeFile(fileID, user, destinationFolder, clientIDToShare);
	}

	public static ArrayList<String> getFileDesc(User user) {
		return GetFileDesc.getFileDesc(user);
	}

	public static String readFile(String fileGID, User user) {
		return ReadFile.readFile(false, fileGID, user);
	}
	public static String readFile(String fileGID, User user, String destinationFolder) {
		return ReadFile.readFile(false, fileGID, user, destinationFolder);
	}
	
	public static void shareFile(String fileGID, int userGID, User myInfo) throws UnknownHostException, IOException {
		ShareFile.shareFile(fileGID, userGID, myInfo);
	}
}
