#!/bin/bash

if [ $# -lt 1 ] || [ $# -gt 1 ]; then
    echo "Numero client ?"
    exit 1
fi

# $1 = numero client
let port=33000+$1

# lancement tokens
konsole --noclose --workdir "~/SIPD/machines/Token$1" -e sh -c "../../init/NetDBMS -p $port"

sleep 10

# InitDB
/usr/lib/jvm/java-8-oracle/bin/java -DconfigurationFilePath=/home/user/SIPD/machines/Token$1/initdb.conf -Dfile.encoding=ISO-8859-1 -classpath /home/user/SIPD/src/tcell/bin:/home/user/SIPD/src/core/bin:/home/user/SIPD/src/core/lib/bouncycastle-jce-jdk13-112.jar:/home/user/SIPD/src/JDBC/classes:/home/user/SIPD/src/JDBC/class_include/commons-io-2.4.jar:/home/user/SIPD/src/JDBC/class_include/commons-logging-1.1.3.jar:/home/user/SIPD/src/JDBC/class_include/httpclient-4.3.6.jar:/home/user/SIPD/src/JDBC/class_include/httpcore-4.3.3.jar:/home/user/SIPD/src/JDBC/class_include/json-simple-1.1.1.jar:/home/user/SIPD/JDK/jdk1.8.0_51/jre/lib/jce.jar database.DatabaseMain

sleep 10

# Daemon
/usr/lib/jvm/java-8-oracle/bin/java -DconfigurationFilePath=/home/user/SIPD/machines/Tcell$1/daemon.conf -Dfile.encoding=ISO-8859-1 -classpath /home/user/SIPD/src/tcell/bin:/home/user/SIPD/src/core/bin:/home/user/SIPD/src/core/lib/bouncycastle-jce-jdk13-112.jar:/home/user/SIPD/src/JDBC/classes:/home/user/SIPD/src/JDBC/class_include/commons-io-2.4.jar:/home/user/SIPD/src/JDBC/class_include/commons-logging-1.1.3.jar:/home/user/SIPD/src/JDBC/class_include/httpclient-4.3.6.jar:/home/user/SIPD/src/JDBC/class_include/httpcore-4.3.3.jar:/home/user/SIPD/src/JDBC/class_include/json-simple-1.1.1.jar:/home/user/SIPD/JDK/jdk1.8.0_51/jre/lib/jce.jar daemon.TCellDaemon
